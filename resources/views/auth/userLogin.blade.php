@extends('layouts.main')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">Login</h1>
          </div>
        </div>
      </div>
    </div>
		
		<section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-8 ftco-animate">
			<form method="POST" action="{{ route('user.login') }}" class="billing-form bg-light p-3 p-md-5">
            {{ csrf_field() }}
				<h3 class="mb-4 billing-heading">Login</h3>
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
	          	<div class="row align-items-end">
	          		<div class="col-md-12">
	                <div class="form-group">
	                	<label for="email">Email</label>
	                  <input name="email" type="text" class="form-control" placeholder="eg: jack@dean.com"  value="{{ old('email') }}">
	                </div>
	              </div>
	              <div class="col-md-12">
	                <div class="form-group">
	                	<label for="lastname">Password</label>
	                  <input name="password" type="password" class="form-control">
	                </div>
                </div>

                <div class="form-group">
                  <button class="btn btn-primary py-3 px-4">Login</button>
                <div>
                </div>
	            </div>
	          </form><!-- END -->
          </div> <!-- .col-md-8 -->
        </div>
      </div>
    </section> <!-- .section -->

    @endsection