@extends('layouts.main')

@section('content')
		<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">{{ App::getLocale() == 'en' ? 'My Cart' : 'Mio Carello'}}</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ App::getLocale() == 'en' ? '/' : '/it/' }}">Home</a></span> <span>Cart</span></p>
          </div>
        </div>
      </div>
    </div>
		
		<section class="ftco-section ftco-cart">
			<div class="container">
				<div class="row">
    			<div class="col-md-12 ftco-animate">
				@if($products->count() > 0)
    				<div class="cart-list">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr class="text-center">
						        <th>&nbsp;</th>
						        <th>&nbsp;</th>
						        <th>Product</th>
								<th>Price</th>
								<th>&nbsp;</th>
								<th>Quantity</th>
								<th>&nbsp;</th>
						        <th>Total</th>
						      </tr>
						    </thead>
						    <tbody>
                            @foreach($products as $product)
						      <tr class="text-center">
						        <td class="product-remove"><a href="#"><span class="ion-ios-close"></span></a></td>
						        
						        <td class="image-prod"><div class="img" style="background-image:url({{ $product->image }});"></div></td>
						        
						        <td class="product-name">
						        	<h3>{{ App::getLocale() == 'it' ? $product->name_it : $product->name_en }}</h3>
						        	<!-- <p>Far far away, behind the word mountains, far from the countries</p> -->
						        </td>
						        
								<td class="price">${{ $product->price }}</td>
								
								<td>
							  		<a class="btn btn-primary py-3 px-4" onclick="removeFromCart({{ $product->id }})">
									  <span class="ion-ios-remove"></span>
									</a>
								</td>						        
						        <td class="quantity">
						        	<div class="input-group mb-3 mx-5 px-5">
                                     <div id="quantity{{$product->id}}" class="quantity">{{ $product->quantity }}</div>
					          	</div>
							  </td>
							  
							  <td>
							  <a class="btn btn-primary py-3 px-4" onclick="addToCart({{ $product->id }})"><span class="ion-ios-add"></span></a>
								</td>
						        <td class="total" id="price{{$product->id}}">€{{ $product->price*$product->quantity }}</td>
                              </tr><!-- END TR-->
                              @endforeach
						    </tbody>
                          </table>
					  </div>
    			</div>
    		</div>
    		<div class="row justify-content-end">
    			<div class="col col-lg-5 col-md-6 mt-5 cart-wrap ftco-animate">
    				<div class="cart-total mb-3">
    					<h3>Cart Totals</h3>
    					<p class="d-flex">
    						<span>Subtotal</span>
    						<span id="subtotal">€{{ $total }}</span>
    					</p>
    					<p class="d-flex">
    						<span>Delivery</span>
    						<span>€5.00</span>
    					</p>
    					<hr>
    					<p class="d-flex total-price">
    						<span>{{ App::getLocale() == 'en' ? 'Total' : 'Totale'}}</span>
    						<span id="total">€{{ $total + 5 }}</span>
    					</p>
					</div>
    				<a href="{{ App::getLocale() == 'en' ? '/checkout' : '/it/checkout' }}" class="text-center"><button type="submit" class="btn btn-primary py-3 px-4">Proceed to Checkout</button></a>
					<!-- <form method="POST" action="{{ route('checkout') }}">
						@csrf
						<input type="hidden" name="total" value="{{ $total }}" />
    					<p class="text-center"><button type="submit" class="btn btn-primary py-3 px-4">Proceed to Checkout</button></p>
					</form> -->
    			</div>
    		</div>
			</div>
			@else
            	<h3>{{ App::getLocale() == 'en' ? 'Your cart is empty.' : 'Il tuo carrello è vuoto.'}}</h3>
        	@endif
		</section>

	
	
<script type="text/javascript">

	function addToCart(product){
		$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
		});

		$.ajax({
			type:'POST',
			url: '/cart/' + product,
			data: "{{ csrf_token() }}",
			success: function(data) {
				$("#quantity" + product).html(data.quantity);
				$("#price" + product).html("$" + data.product_total);
				$("#subtotal").html("$" + data.total);
				$("#total").html("$" + (data.total + 10));
			}
		});
	}

	function removeFromCart(product){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

		$.ajax({
		type:'DELETE',
		url: '/cart/' + product,
		data: "{{ csrf_token() }}",
		success: function(data) {
			if(data.quantity){
				$("#quantity" + product).html(data.quantity);
				$("#price" + product).html("$" + data.product_total);
				$("#subtotal").html("$" + data.total);
				$("#total").html("$" + (data.total + 10));
			}
			else
				location.reload(true);
		}
	});
	}
</script>
@endsection