@extends('layouts.admin')

@section('content')
<div class="container-fluid mt--7">

      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
<div class="order-xl-1">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-3">Add New Product</h3>
                  @if ($errors->any())
                  <div class="alert alert-warning">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
              @method('patch')
              {{ csrf_field() }}
                <h6 class="heading-small text-muted mb-4">Product information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">English Name</label>
                        <input type="text" value="{{ $product->name_en }}" name="name_en" id="input-username" class="form-control form-control-alternative" placeholder="English Name">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Italian Name</label>
                        <input type="text" value="{{ $product->name_it }}" name="name_it" id="input-username" class="form-control form-control-alternative" placeholder="Italian Name">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Price</label>
                        <input type="number" value="{{ $product->price }}" name="price" id="input-username" class="form-control form-control-alternative" placeholder="Price">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label">Category</label>
                        <select name="category" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ $product->category_id == $category->id ? "selected" : "" }}>{{ $category->name_en }}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                        <label class="form-control-label">English Description</label>
                        <textarea name="desc_en" class="form-control form-control-alternative" rows="3" placeholder="Describe your product">{{ $product->desc_en }}</textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                        <label class="form-control-label">Italian Description</label>
                        <textarea name="desc_it" class="form-control form-control-alternative" rows="3" placeholder="Describe your product">{{ $product->desc_it }}</textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Image</label>
                        <input type="file" name="image" id="input-city" class="form-control form-control-alternative">
                      </div>
                    </div>
                </div>
                <div class="row">
                    <button class="btn btn-lg btn-primary">Update Product</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      
</div>
          </div>
        </div>
      </div>
@endsection