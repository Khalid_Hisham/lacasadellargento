@extends('layouts.admin')

@section('content')
<div class="container-fluid mt--7">

      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
<div class="order-xl-1">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-3">Change cover image</h3>
                  @if ($errors->any())
                  <div class="alert alert-warning">
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                @endif
                </div>
              </div>
            </div>
            <div class="card-body">
              <form action="{{ route('admin.cover') }}" method="POST" enctype="multipart/form-data">
              @method('patch')
              {{ csrf_field() }}
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label">Image</label>
                        <input type="file" name="image" id="input-city" class="form-control form-control-alternative">
                      </div>
                    </div>
                </div>
                <div class="row">
                    <button class="btn btn-lg btn-primary">Update cover image</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      
</div>
          </div>
        </div>
      </div>
@endsection