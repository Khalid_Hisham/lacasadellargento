@extends('layouts.admin')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Categories</h3> <a href="{{ route('admin.categories.add') }}" style="float:right" class="btn btn-info">Add Category</a>
            </div>
<div class="table-responsive">
    <div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th scope="col">
                    Image
                </th>
                <th scope="col">
                    English Name
                </th>
                <th scope="col">
                    Italian Name
                </th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach($categories as $category)
            <tr>
                <td class="col-md-auto">
                    <div class="mr-3 media align-items-center">
                          <img style="width:30%;height:30%" alt="Image placeholder" src="{{ $category->image }}">
                    </div>
                </td>
                <td class="budget name">
                    {{ $category->name_en }}
                </td>
                <td class="budget name">
                    {{ $category->name_it }}
                </td>
                <td class="text-right">
                    <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <a class="dropdown-item" href="{{ route('categories.edit', ['category' => $category->id ]) }}">Edit</a>
                            <button class="dropdown-item" type="button" data-toggle="modal" data-target="#exampleModal-{{ $category->id }}">Delete</button>
                            <a class="dropdown-item" href="{{ route('admin.categories.products', ['category' => $category->id]) }}">View Products</a>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal-{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Warning!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    If you delete this category, all associated products will be without a category.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    
                                    <form id="delete" action="{{ route('admin.categories.delete', ['category' => $category->id]) }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-warning">Delete</button>
                                    </form>
                                </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

</div>

</div>
          </div>
        </div>
      </div>
@endsection