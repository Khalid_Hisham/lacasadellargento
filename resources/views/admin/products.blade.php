@extends('layouts.admin')

@section('content')
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Products</h3> <a href="{{ route('admin.products.add') }}" style="float:right" class="btn btn-info">Add Product</a>
            </div>
            <div class="table-responsive">
    <div>
    <table class="table align-items-center">
        <thead class="thead-light">
            <tr>
                <th scope="col">
                    English Name
                </th>
                <th scope="col">
                    Italian Name
                </th>
                <th scope="col">
                    English Description
                </th>
                <th scope="col">Italian Description</th>
                <th scope="col">Price</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody class="list">
            @foreach($products as $product)
            <tr>
                <th scope="row" class="name">
                    <div class="media align-items-center">
                          <img style="width:30%;height:30%" alt="Image placeholder" src="{{ $product->image }}">
                        <div class="media-body">
                            <span class="mb-0 ml-2 text-sm">{{ $product->name_en }}</span>
                        </div>
                    </div>
                </th>
                <td class="name">
                    {{ $product->name_it }}
                </td>
                <td class="name">
                    {{ strlen($product->desc_en) > 50 ? substr($product->desc_en, 0, 49) . "..." : $product->desc_en }}
                </td>
                <td class="name">
                    {{ strlen($product->desc_it) > 50 ? substr($product->desc_it, 0, 49) . "..." : $product->desc_it }}
                </td>
                <td class="budget">
                    €{{ $product->price }} Euros
                </td>
                <td class="text-right">
                    <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                            <form action="{{ route('products.stock', ['product' => $product->id]) }}" method="post">
                                @method('put')
                                @csrf
                                <button class="dropdown-item" type="submit">{{ $product->stock ? 'Out of stock' : 'In stock' }}</button>
                            </form>
                            <a class="dropdown-item" href="{{ route('products.edit', ['product' => $product->id]) }}">Edit</a>
                            <form action="{{ route('admin.products.delete', ['product' => $product->id]) }}" method="post">
                                @method('delete')
                                @csrf
                                <button class="dropdown-item" type="submit">Delete</button>
                            </form>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

</div>
</div>
          </div>
        </div>
      </div>
@endsection