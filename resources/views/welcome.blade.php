@extends('layouts.main')

@section('content')
        <!-- <div class="hero-wrap js-fullheight" style="background-image: url('images/bg_1.jpg');"> -->
        <div class="hero-wrap js-fullheight" style="background-image: url({{ $cover->image }});">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
        	<h3 class="v">Lacasadell’argento</h3>
        	<!-- <h3 class="vr">Since - 1985</h3> -->
          <div class="col-md-11 ftco-animate text-center">
            <h1 style="font-size:35px">Lacasadell’argento</h1>
            <h2><span>Wear Your Accessories</span></h2>
          </div>
          <div class="mouse">
						<a href="#" class="mouse-icon">
							<div class="mouse-wheel"><span class="ion-ios-arrow-down"></span></div>
						</a>
					</div>
        </div>
      </div>
    </div>

    <div class="goto-here"></div>
    
    <section class="ftco-section ftco-product">
    	<div class="container">
    		<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<h1 class="big">Categories</h1>
            <h2 class="mb-4">Categories</h2>
          </div>
        </div>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="product-slider owl-carousel ftco-animate">
					@foreach($categories as $category)
					<div class="item">
		    				<div class="product">
		    					<a href="{{ App::getLocale() == 'en' ? '/' : 'it/' }}category/{{ $category->id }}" class="img-prod"><img class="img-fluid" src="{{ $category->image }}" alt="Colorlib Template">
		    					</a>
		    					<div class="text pt-3 px-3">
		    						<h3><a href="#">{{ App::getLocale() == 'it' ? $category->name_it : $category->name_en }}</a></h3>
		    						<div class="d-flex">
		    						</div>
		    					</div>
		    				</div>
	    				</div>
					@endforeach
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ $about->image }});">
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-5 mt-md-5">
	          	<div class="ml-md-0">
		            <h2 style="font-size:35px" class="mb-4">Lacasadell’argento <br>Online <br> <span>Shop</span></h2>
	            </div>
	          </div>
	          <div class="pb-md-5">
              {{ App::getLocale() == 'it' ? $about->about_it : $about->about_en }}
						</div>
					</div>
				</div>
			</div>
		</section>

    <section class="ftco-section bg-light">
    	<div class="container">
				<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<h1 class="big">Products</h1>
            <h2 class="mb-4">Our Products</h2>
          </div>
        </div>    		
    	</div>
    	<div class="container-fluid">
    		<div class="row">
			@foreach($products as $product)
			<div class="col-sm col-md-6 col-lg ftco-animate">
    				<div class="product">
    					<a href="{{ App::getLocale() == 'en' ? '/' : 'it/' }}product/{{ $product->id }}" class="img-prod"><img class="img-fluid" src="{{ $product->image }}" alt="Colorlib Template"></a>
    					<div class="text py-3 px-3">
    						<h3><a href="{{ App::getLocale() == 'en' ? '/' : 'it/' }}product/{{ $product->id }}">{{ App::getLocale() == 'it' ? $product->name_it : $product->name_en }}</a></h3>
    						<div class="d-flex">
    							<div class="pricing">
		    						<p class="price"><span>${{ $product->price }}</span></p>
		    					</div>
	    					</div>
	    					<hr>
    						<p id="cart{{ $product->id }}" class="bottom-area d-flex">
    							@if(array_key_exists($product->id, $cart))
                    <button class="btn btn-primary py-3 px-4 mx-5" onClick="removeFromCart({{ $product->id }})">
                      <span class="ion-ios-remove"></span>
                    </button>
                      {{$cart[$product->id]}}
                    <button class="btn btn-primary py-3 px-4 mx-5" onClick="addToCart({{ $product->id }})">
                      <span class="ion-ios-add"></span>
                    </button>
                  @else
                    <a class="btn btn-primary py-3 px-4" onclick="addToCart({{ $product->id }})" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                  @endif
    						</p>
    					</div>
    				</div>
    			</div>
			@endforeach
    		</div>
    	</div>
      <div id="success" style="display: none" class="alert alert-success"></div>
    </section>


<script type="text/javascript">

	function addToCart(product){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
		});

		$.ajax({
      type:'POST',
      url: '/cart/' + product,
      data: "{{ csrf_token() }}",
      success: function(data) {
        $("#cart" + product).html(data.html);
        $("#success").html(data.success);
        $("#success").css("display", "block");
      }
		});
	}

  function removeFromCart(product){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        	}
		});

		$.ajax({
      type:'DELETE',
      url: '/cart/' + product,
      data: "{{ csrf_token() }}",
      success: function(data) {
        $("#cart" + product).html(data.html);
        $("#success").html(data.success);
        $("#success").css("display", "block");
      }
		});
	}
</script>

    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/bg_4.jpg);">
    	<div class="container">
    		<div class="row justify-content-center py-5">
    			<div class="col-md-10">
		    		<div class="row">
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="{{ $users_count }}">0</strong>
		                <span>Happy Customers</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="{{ $products_count }}">0</strong>
		                <span>Products</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="{{ $orders_count }}">0</strong>
		                <span>Orders</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="{{ $reviews_count }}">0</strong>
		                <span>Positive Reviews</span>
		              </div>
		            </div>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section bg-light ftco-services">
    	<div class="container">
    		<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
            <h1 class="big">Services</h1>
            <h2>what makes us different</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
            		<span class="flaticon-002-recommended"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">worldwide shipping</h3>
                <p>Get your order wherever you live in a short period of time.</p>
              </div>
            </div>      
          </div>
          <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
            		<span class="flaticon-001-box"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Premium Packaging</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
              </div>
            </div>    
          </div>
          <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="icon d-flex justify-content-center align-items-center mb-4">
            		<span class="flaticon-003-medal"></span>
              </div>
              <div class="media-body">
                <h3 class="heading">Superior Quality</h3>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
              </div>
            </div>      
          </div>
        </div>
    	</div>
    </section>
@endsection
