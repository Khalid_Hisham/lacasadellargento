@extends('layouts.main')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">{{ $locale['name'] }}</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ App::getLocale() == 'en' ? '/' : '/it/' }}">Home</a></span></p>
          </div>
        </div>
      </div>
    </div>
		
		<section class="ftco-section bg-light">
    	<div class="container">
    		<div class="row">
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="{{ $product->image }}" class="image-popup"><img src="{{ $product->image }}" class="img-fluid" alt="Colorlib Template"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3>{{ $locale['name'] }}</h3>
    				<p class="price"><span>${{ $product->price }}</span></p>
    				<p>{{ $locale['desc'] }}</p>
    			</div>
    		</div>
			<p id="cart{{ $product->id }}" class="bottom-area d-flex">
    				@if(array_key_exists($product->id, $cart))
                    <button class="btn btn-primary py-3 px-4 mx-5" onClick="removeFromCart({{ $product->id }})">
                      <span class="ion-ios-remove"></span>
                    </button>
                      {{$cart[$product->id]}}
                    <button class="btn btn-primary py-3 px-4 mx-5" onClick="addToCart({{ $product->id }})">
                      <span class="ion-ios-add"></span>
                    </button>
					@else
						<a class="btn btn-primary py-3 px-4" onclick="addToCart({{ $product->id }})" class="add-to-cart"><span>{{ App::getLocale() == 'it' ? 'Aggiungi al carrello' : 'Add to cart' }}<i class="ion-ios-add ml-1"></i></span></a>
					@endif
    				</p>
    	</div>
		<div id="success" style="display: none" class="alert alert-success"></div>
    </section>


<script type="text/javascript">

	function addToCart(product){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
	type:'POST',
	url: '/cart/' + product,
	data: "{{ csrf_token() }}",
	success: function(data) {
		$("#cart" + product).html(data.html);
		$("#success").html(data.success);
		$("#success").css("display", "block");
	}
		});
	}

	function removeFromCart(product){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
	type:'DELETE',
	url: '/cart/' + product,
	data: "{{ csrf_token() }}",
	success: function(data) {
		$("#cart" + product).html(data.html);
		$("#success").html(data.success);
		$("#success").css("display", "block");
	}
		});
	}
</script>
@endsection