@extends('layouts.main')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">About Us</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ App::getLocale() == 'it' ? '/it' : '/' }}">Home</a></span> <span>About</span></p>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ $about->image }});">
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-5 mt-md-5">
	          	<div class="ml-md-0">
		            <h2 class="mb-4">Lacasadell’argento <br>Online <br> <span>Shop</span></h2>
	            </div>
	          </div>
	          <div class="pb-md-5">
              {{ App::getLocale() == 'it' ? $about->about_it : $about->about_en }}
						</div>
					</div>
				</div>
			</div>
		</section>
        @endsection
