@extends('layouts.main')

@section('content')
<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">Categories</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ App::getLocale() == 'it' ? '/it' : '/' }}">Home</a></span></p>
          </div>
        </div>
      </div>
    </div>
		
		<section class="ftco-section bg-light">
    	<div class="container-fluid">
    		<div class="row">
          @foreach($categories as $category)
          <div class="col-sm col-md-6 col-lg-3 ftco-animate">
    				<div class="product">
                    <a href="{{ App::getLocale() == 'en' ? '/' : 'it/' }}category/{{ $category->id }}" class="img-prod"><img class="img-fluid" src="{{ $category->image }}" alt="Colorlib Template">
    					</a>
    					<div class="text py-3 px-3">
    						<h3><a href="#">{{ App::getLocale() == 'it' ? $category->name_it : $category->name_en }}</a></h3>
	    					<hr>
    						</p>
    					</div>
    				</div>
    			</div>
          @endforeach
    		</div>
        </div>
    	</div>
    </section>
@endsection