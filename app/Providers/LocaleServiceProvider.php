<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;
use Illuminate\Http\Request;


class LocaleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        if($request->segment(1) == 'it') {
            $locale = 'it';
        } else {
            $locale = 'en';
        }
    
        App::setLocale($locale);
    }
}
