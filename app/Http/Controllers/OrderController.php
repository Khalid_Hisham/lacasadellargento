<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use App\Models\Order;
use App\Models\Product;
use App;

class OrderController extends Controller
{
    private $_api_context;

    public function __construct()
    {
 
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
 
    }

    public function unfinishedOrders()
    {
        return view('admin.unfinished_orders', [
            'orders' => Order::where('status', 0)->get()
        ]);
    }

    public function orderProducts(Order $order)
    {
        return view('admin.order_products', [
            'products' => $order->products
        ]);
    }

    public function payWithpaypal(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'phone' => 'required'
        ]);

        $order = new Order;

        $order->first_name = $request->first_name;
        $order->last_name = $request->last_name;
        $order->email = $request->email;
        $order->address = $request->address;
        $order->address2 = $request->address2 ? $request->address2 : null;
        $order->city = $request->city;
        $order->zipcode = $request->zipcode;
        $order->phone = $request->phone;
        $order->total = $request->total;
        $order->orderID = uniqid();

        session()->put('order', $order);
 
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
 
        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($request->total);// Session total
 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setDescription('Please pay ' . $amount->total);
 
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
            ->setCancelUrl(URL::route('status'));
 
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                session()->put('error', 'Connection timeout');
                return Redirect::route('paywithpaypal');
            } else {
                session()->put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');
            }
        }
 
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
 
        /** add payment ID to session **/
        session()->put('paypal_payment_id', $payment->getId());
 
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
 
        session()->put('error', 'Unknown error occurred');
        return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = session()->get('paypal_payment_id');
 
        /** clear the session payment ID **/
        session()->forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
 
            session()->put('error', 'Payment failed');
            return Redirect::route('home');
 
        }
 
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
 
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
 
        if ($result->getState() == 'approved') {
            $order = session('order');
            $order->save();
            $products = Product::whereIn('id', array_keys(session('cart')))->get();
            foreach ($products as $product) {
                $order->products()->attach($product, ['quantity' => session('cart')[$product->id], 'price' => $product->price]);
            }
            session()->flush();
            session()->put('success', 'Payment success');
            if(App::getLocale() == 'it'){
                return redirect('/it');
            } else {
                return redirect('/');
            }
        }
 
        session()->put('error', 'Payment failed');
        if(App::getLocale() == 'it'){
            return redirect('/it');
        } else {
            return redirect('/');
        }
    }

    public function done(Order $order)
    {
        $order->update([
            'status' => 1
        ]);

        return redirect()->back();
    }
}
