<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends Controller
{
    public function show()
    {
      $products = collect();
      $total = 0;
      if (session()->has('cart')) {
        $products = Product::whereIn('id', array_keys(session('cart')))->get();
        foreach ($products as $product) {
          $product->quantity = session('cart')[$product->id];
          $total += $product->price * $product->quantity;
        }
      }

      return view('cart', [
        'products' => $products,
        'total' => $total
      ]);
    }

    public function addToCart(Product $product)
    {
      if (session()->has('cart')) {
        $cart = session('cart');
        
        if (array_key_exists($product->id, $cart))
          $cart[$product->id]++;
        else
          $cart[$product->id] = 1;

        session()->put('cart', $cart);
        
      } else {
        session()->put('cart', [$product->id => 1]);
      }

      $products = collect();
      $total = 0;
      if (session()->has('cart')) {
        $products = Product::whereIn('id', array_keys(session('cart')))->get();
        foreach ($products as $cart_product) {
          $cart_product->quantity = session('cart')[$cart_product->id];
          $total += $cart_product->price * $cart_product->quantity;
        }
      }

      $cart = session('cart');
      return response()->json([
        'success' => 'Product added to cart successfully!',
        'html' => '<button class="btn btn-primary py-3 px-4 mx-5" onClick="removeFromCart(' . $product->id . ')">
                    <span class="ion-ios-remove"></span>
                  </button>
                  ' . $cart[$product->id] . '
                  <button class="btn btn-primary py-3 px-4 mx-5" onClick="addToCart(' . $product->id . ')">
                    <span class="ion-ios-add"></span>
                  </button>',
        'quantity' => $cart[$product->id],
        'product_total' => $cart[$product->id]*$product->price,
        'total' => $total
      ]);
    }

    public function removeFromCart(Product $product, Request $request)
    {
      if (session()->has('cart')) {
        $cart = session('cart');
        
        if (array_key_exists($product->id, $cart)) {
          if ($request->has('delete-all') || $cart[$product->id] <= 1)
          {
            unset($cart[$product->id]);
            session()->put('cart', $cart);
            return response()->json([
              'success' => 'Product removed from cart successfully!',
              'html' => '<a class="btn btn-primary py-3 px-4" onclick="addToCart(' . $product->id . ')" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>'
            ]);
          }
          else
          {
            $cart[$product->id]--;
            session()->put('cart', $cart);

            $products = collect();
            $total = 0;
            if (session()->has('cart')) {
              $products = Product::whereIn('id', array_keys(session('cart')))->get();
              foreach ($products as $cart_product) {
                $cart_product->quantity = session('cart')[$cart_product->id];
                $total += $cart_product->price * $cart_product->quantity;
              }
            }

            return response()->json([
              'success' => 'Product quantity reduced in the cart successfully!',
              'html' => '<button class="btn btn-primary py-3 px-4 mx-5" onClick="removeFromCart(' . $product->id . ')">
                    <span class="ion-ios-remove"></span>
                  </button>
                  ' . $cart[$product->id] . '
                  <button class="btn btn-primary py-3 px-4 mx-5" onClick="addToCart(' . $product->id . ')">
                    <span class="ion-ios-add"></span>
                  </button>',
              'quantity' => $cart[$product->id],
              'product_total' => $cart[$product->id]*$product->price,
              'total' => $total
            ]);
          }
        } else {
          return redirect()->back()->with('success', 'Product is not in the cart.');
        }
      } else {
        return redirect()->back()->with('warning', 'Cart is empty!');
      }
    }

    public function checkout()
    {
      $products = collect();
      $total = 0;
      if (session()->has('cart')) {
        $products = Product::whereIn('id', array_keys(session('cart')))->get();
        foreach ($products as $product) {
          $product->quantity = session('cart')[$product->id];
          $total += $product->price * $product->quantity;
        }
      }

      return view('checkout', [
        'products' => $products,
        'total' => $total
      ]);
    }
}
