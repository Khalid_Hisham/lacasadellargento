<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App;

class ProductsController extends Controller
{
    public function index()
    {
        return view('admin.products', [
            'products' => Product::all()
        ]);
    }

    public function add()
    {
        return view('admin.productsAdd', [
            'categories' => Category::all()
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required',
            'name_it' => 'required',
            'desc_en' => 'required',
            'desc_it' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        $product = new Product([
            "name_en" => $request['name_en'],
            "name_it" => $request['name_it'],
            "desc_en" => $request['desc_en'],
            "desc_it" => $request['desc_it'],
            "price" => $request['price'],
            "image" => "/images/" . $imageName
        ]);

        $product->category()->associate($request['category']);

        $product->save();

        return redirect(route('admin.products'));
    }

    public function edit(Product $product)
    {
        return view('admin.productsEdit' ,[
            'product' => $product,
            'categories' => Category::all()
        ]);
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request, [
            'name_en' => 'required',
            'name_it' => 'required',
            'desc_en' => 'required',
            'desc_it' => 'required',
            'price' => 'required|numeric',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        @unlink(public_path() . $product->image);

        $product->category()->associate($request['category']);

        $product->update([
            "name_en" => $request['name_en'],
            "name_it" => $request['name_it'],
            "desc_en" => $request['desc_en'],
            "desc_it" => $request['desc_it'],
            "price" => $request['price'],
            "image" => "/images/" . $imageName
        ]);

        return redirect(route('admin.products'));
    }

    public function show(Product $product)
    {
        $cart = array();
        if (session()->has('cart')) 
        {
            $cart = session('cart');
        }

        $localeProduct = new \stdClass();

        if(App::getLocale() == 'it')
        {
            $localeProduct->name = $product->name_it;
            $localeProduct->desc = $product->desc_it;
        } else if (App::getLocale() == 'en')
        {
            $localeProduct->name = $product->name_en;
            $localeProduct->desc = $product->desc_en;
        }

        $localeProduct = json_encode($localeProduct);

        $localeProduct = json_decode($localeProduct, true);

        return view('product', [
            'product' => $product,
            'locale' => $localeProduct,
            'cart' => $cart
        ]);
    }

    public function delete(Product $product)
    {
        $product->delete();

        return redirect()->back();
    }

    public function stock(Product $product)
    {
        $product->update([
            'stock' => !$product->stock
        ]);

        return redirect()->back();
    }
}
