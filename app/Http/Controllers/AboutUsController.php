<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AboutUs;

class AboutUsController extends Controller
{
    public function show()
    {
        return view('about', [
            'about' => AboutUs::find(1)
        ]);
    }

    public function edit()
    {
        return view('admin.about', [
            'about' => AboutUs::find(1)
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'about_en' => 'required',
            'about_it' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $about = AboutUs::find(1);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        @unlink(public_path() . $about->image);

        $about->update([
            "about_en" => $request['about_en'],
            "about_it" => $request['about_it'],
            "image" => "/images/" . $imageName
        ]);

        return redirect(route('admin.index'));
    }
}
