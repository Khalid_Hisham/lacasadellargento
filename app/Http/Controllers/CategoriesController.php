<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use DB;

class CategoriesController extends Controller
{
    public function index()
    {
        return view('admin.categories', [
            'categories' => Category::all()
        ]);
    }

    public function customerIndex()
    {
        return view('categories', [
            'categories' => Category::all()
        ]);
    }

    public function show(Category $category)
    {
        $cart = array();
        if (session()->has('cart')) {
            $cart = session('cart');
        }
        return view('shop', [
            'category' => $category,
            'products' => $category->products()->where('stock', 1)->get(),
            'cart' => $cart
        ]);
    }

    public function add()
    {
        return view('admin.categoriesAdd');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_en' => 'required',
            'name_it' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        $category = new Category([
            "name_en" => $request['name_en'],
            "name_it" => $request['name_it'],
            "image" => "/images/" . $imageName
        ]);

        $category->save();

        return redirect(route('admin.categories'));
    }

    public function edit(Category $category)
    {
        return view('admin.categoriesEdit', [
            'category' => $category
        ]);
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'name_en' => 'required',
            'name_it' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        @unlink(public_path() . $category->image);

        $category->update([
            "name_en" => $request['name_en'],
            "name_it" => $request['name_it'],
            "image" => "/images/" . $imageName
        ]);

        return redirect(route('admin.categories'));
    }

    public function products(Category $category)
    {
        return view('admin.categoryProducts', [
            'products' => $category->products()->get()
        ]);
    }

    public function delete(Category $category)
    {
        DB::table('products')->where('category_id', $category->id)->update(['category_id' => null]);
        $category->delete();

        return redirect(route('admin.categories'));
    }
}
