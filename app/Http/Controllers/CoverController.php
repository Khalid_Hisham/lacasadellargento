<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cover;

class CoverController extends Controller
{
    public function changeCover(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $cover = Cover::find(1);

        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        $request->image->move(public_path('images'), $imageName);

        @unlink(public_path() . $product->image);
        
        $cover->update([
            "image" => "/images/" . $imageName
        ]);

        return redirect()->route('admin.index');
    }
}
