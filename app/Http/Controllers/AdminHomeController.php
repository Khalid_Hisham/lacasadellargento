<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;

class AdminHomeController extends Controller
{
    public function index()
    {
        return view('admin.index', [
            'orders' => Order::all(),
        ]);
    }    
}