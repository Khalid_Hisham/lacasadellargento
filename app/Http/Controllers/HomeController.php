<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use App\Models\Cover;
use App\Models\AboutUs;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cart = array();
        if (session()->has('cart')) {
            $cart = session('cart');
        }

        return view('welcome', [
            'categories' => Category::all(),
            'products' => Product::orderBy('created_at', 'desc')->where('stock', 1)->take(4)->get(),
            'cart' => $cart,
            'users_count' => 100,
            'products_count' => 500,
            'orders_count' => 2000,
            'reviews_count' => 500,
            'cover' => Cover::find(1),
            'about' => AboutUs::find(1)
            // 'users_count' => User::all()->count(),
            // 'products_count' => Product::all()->count(),
            // 'orders_count' => Order::all()->count(),
            // 'reviews_count' => Review::all()->count()
        ]);
    }
}
