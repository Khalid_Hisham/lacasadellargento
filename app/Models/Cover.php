<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $guarded = [];

    protected $table = 'cover';
}
