<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ["name_en", "name_it", "image"];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
