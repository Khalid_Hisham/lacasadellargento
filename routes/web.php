<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

include('routes.php');

Route::group(['prefix' => 'it'], function () {
    include('routes.php');
});

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/login/user', 'Auth\LoginController@showUserLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');
Route::get('/register/user', 'Auth\RegisterController@showUserRegisterForm');

Route::post('/login/admin', 'Auth\LoginController@adminLogin')->name('admin.login');;
Route::post('/login/user', 'Auth\LoginController@userLogin')->name('user.login');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');
Route::post('/register/user', 'Auth\RegisterController@createUser');

Route::get('/admin', 'AdminHomeController@index')->name('admin.index')->middleware('auth:admin');

Route::get('/admin/products', 'ProductsController@index')->name('admin.products')->middleware('auth:admin');
Route::delete('/admin/products/{product}', 'ProductsController@delete')->name('admin.products.delete')->middleware('auth:admin');
Route::get('/admin/products/add', 'ProductsController@add')->name('admin.products.add')->middleware('auth:admin');
Route::post('/admin/products', 'ProductsController@store')->name('products.add')->middleware('auth:admin');
Route::get('/admin/products/edit/{product}', 'ProductsController@edit')->name('products.edit')->middleware('auth:admin');
Route::patch('/admin/products/{product}', 'ProductsController@update')->name('products.update')->middleware('auth:admin');
Route::put('/admin/products/{product}/stock', 'ProductsController@stock')->name('products.stock')->middleware('auth:admin');

Route::get('/admin/categories', 'CategoriesController@index')->name('admin.categories')->middleware('auth:admin');
Route::get('/admin/categories/add', 'CategoriesController@add')->name('admin.categories.add')->middleware('auth:admin');
Route::post('/admin/categories', 'CategoriesController@store')->name('categories.add')->middleware('auth:admin');
Route::delete('/admin/categories/{category}', 'CategoriesController@delete')->name('admin.categories.delete')->middleware('auth:admin');
Route::get('/admin/categories/edit/{category}', 'CategoriesController@edit')->name('categories.edit')->middleware('auth:admin');
Route::patch('/admin/categories/{category}', 'CategoriesController@update')->name('categories.update')->middleware('auth:admin');
Route::get('/admin/categories/{category}/products', 'CategoriesController@products')->name('admin.categories.products')->middleware('auth:admin');

Route::get('/admin/orders', 'OrderController@unfinishedOrders')->name('admin.unfinished_orders')->middleware('auth:admin');
Route::get('/admin/{order}/products', 'OrderController@orderProducts')->name('admin.order_products')->middleware('auth:admin');
Route::put('/admin/orders/{order}/done', 'OrderController@done')->name('orders.done')->middleware('auth:admin');

Route::get('/admin/cover', function () {
    return view('admin.cover');
});
Route::patch('/admin/cover', 'CoverController@changeCover')->name('admin.cover')->middleware('auth:admin');

Route::get('/admin/about', 'AboutUsController@edit')->middleware('auth:admin');
Route::patch('/admin/about', 'AboutUsController@update')->name('admin.about')->middleware('auth:admin');


Route::post('paypal', 'OrderController@payWithpaypal')->name('pay');
// route for check status of the payment
Route::get('status', 'OrderController@getPaymentStatus')->name('status');

Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout')->middleware('auth:admin');