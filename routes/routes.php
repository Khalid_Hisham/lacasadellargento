<?php



Route::get('/', 'HomeController@index')->name('home');
Route::get('/product/{product}', 'ProductsController@show')->name('product');
Route::get('/category/{category}', 'CategoriesController@show')->name('category');
Route::get('/categories', 'CategoriesController@customerIndex');
Route::get('/about', 'AboutUsController@show')->name('about');

Route::get('/cart', 'CartController@show')->name('cart.show');
Route::post('/cart/{product}', 'CartController@addToCart')->name('cart.add');
Route::delete('/cart/{product}', 'CartController@removeFromCart')->name('cart.delete');

Route::get('/checkout', 'CartController@checkout')->name('checkout');